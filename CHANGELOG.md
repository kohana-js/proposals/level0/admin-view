# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.1.1](https://gitlab.com/kohana-js/proposals/level0/admin-view/compare/v1.1.0...v1.1.1) (2023-03-15)

## 1.1.0 (2021-10-28)


### Features

* add pagination snippet ([17ab164](https://gitlab.com/kohana-js/proposals/level0/admin-view/commit/17ab164377a89f68bae618df939b19d0e220f3a5))

## [1.0.5] - 2021-09-07
### Added
- create CHANGELOG.md